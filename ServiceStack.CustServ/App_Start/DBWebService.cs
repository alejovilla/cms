using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using ServiceStack.Common;
using ServiceStack.Common.Web;
using ServiceStack.Configuration;
using ServiceStack.CustServ.Models.Operations;
using ServiceStack.CustServ.Models.Types;
using ServiceStack.OrmLite;
using ServiceStack.ServiceClient.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.Text;
using ServiceStack.WebHost.Endpoints;

namespace ServiceStack.CustServ{

     [Authenticate]

                
    public class DBWebService : ServiceInterface.Service
    {
        public IDbConnectionFactory dbFactory { get; set; } //Injected by IOC


        //Users

        public object Get(GetUser user)
        {
            try
            {
                IAuthSession session = this.GetSession();
                var UserResponse = new User { DisplayName = session.DisplayName, id = session.Id.ToInt(), Permissions = session.Permissions,Roles = session.Roles,UserName = session.UserName };
                this.Request.SaveSession(session, System.TimeSpan.FromMinutes(60*2));
                return UserResponse;
            }
            catch (Exception)
            {

                throw new HttpError(HttpStatusCode.NotFound, "Mabel's CMS", "User {0} does not exist".Fmt(user.Id));
            }
           
        }

        // Colours verbs

        public object Get(Colours request)
        {

            return Db.Select<Colour>();
        }

        public object Get(GetColour request)
        {
            try
            {
                var item = Db.GetById<Colour>(request.Id);
                return item;
            }
            catch (Exception)
            {

                throw new HttpError(HttpStatusCode.NotFound, "Mabel's CMS", "Colour {0} does not exist".Fmt(request.Id));
            }

        }

        public object Get(GetColourMany2ManyTables request)
        {
            try
            {
                var result = new GetColourMany2ManyTablesResponse();
                result.colour = Db.GetById<Colour>(request.Id);

                var consulta = @"SELECT SI.code  as Code ,SI.Id as Id
                                 FROM Standards_Items SI  inner join `StdItems_StdColours` StdI on SI.Code = StdI.itemCode                                                         
                                                         where StdI.colourid =  '{0}'".Fmt(request.Id);
                result.items = Db.Select<Item>(consulta);


                consulta = @"SELECT ST.name  as Name ,ST.Id as Id
                                FROM `StdColours_StdTemplates` StdT 
                                                         inner join Standards_BuilderTemplates ST on StdT.tmplId=ST.Id 
                                                         where StdT.colourid = '{0}'".Fmt(request.Id);
                result.templates = Db.Select<Template>(consulta);

                return result;
            }
            catch (Exception webEx)
            {
                throw new HttpError(HttpStatusCode.NotFound, "Mabel's CMS", "Colour {0} does not exist".Fmt(request.Id));
            }
        }

        public object Get(GetColourMany2ManyLayoutsTable request)
        {
            var consulta = @"SELECT ST.name  as Name ,ST.Id as Id
                                FROM `StdColours_StdTemplates` StdT 
                                                         inner join Standards_BuilderTemplates ST on StdT.tmplId=ST.Id 
                                                         where StdT.colourid = '{0}'".Fmt(request.Id);
            return Db.Select<Template>(consulta);
        }


        public object Get(GetColourMany2ManyItemsTable request)
        {
            var consulta = @"SELECT SI.code  as Code ,SI.Id as Id
                                 FROM Standards_Items SI  inner join `StdItems_StdColours` StdI on SI.Code = StdI.itemCode                                                         
                                                         where StdI.colourid =  '{0}'".Fmt(request.Id);
            return Db.Select<Item>(consulta);
        }

        public object Put(Many2ManyColourUpdate colourTrans)
        {
            using (IDbTransaction dbTrans = Db.OpenTransaction(IsolationLevel.RepeatableRead))
            {
                try
                {
                    Colour colour = new Colour
                    {
                        id   =  colourTrans.Id,
                        Name = colourTrans.Name,
                        Name_en = colourTrans.Name_en,
                        Name_fr = colourTrans.Name_fr,
                        IsActive = colourTrans.IsActive
                    };

                    Db.Delete<Item_Colours>(q => q.colourId == colourTrans.Id);
                    Db.Delete<Templates_Colours>(q => q.colourId == colourTrans.Id);


                    Db.UpdateOnly(colour, q => new { q.Name, q.Name_fr, q.Name_en, q.IsActive }, q => q.id == colour.id);
                    if (!colourTrans.MultiSItems.IsEmpty())
                    {
                        var newItemColours = colourTrans.MultiSItems.Select(itemCode => new Item_Colours { itemCode = itemCode, colourId = colourTrans.Id }).ToList();
                        Db.InsertAll<Item_Colours>(newItemColours);
                    }
                    if (!colourTrans.MultiSLayouts.IsEmpty())
                    {
                        var newLayoutIcons = colourTrans.MultiSLayouts.Select(layoutId => new Templates_Colours { colourId= colourTrans.Id, tmplId = layoutId }).ToList();
                        Db.InsertAll<Templates_Colours>(newLayoutIcons);
                    }

                    dbTrans.Commit();
                    return colourTrans;

                }// try

                catch (Exception webEx)
                {
                    dbTrans.Rollback();
                    throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "Transaction error when updating Many2Many or Colour tables:" + webEx.Message);

                }
            }// transaction

        }

         // Items verbs

        public object Get(Items request)
        {
            return Db.Select<Item>();
        }

        public object Get(GetItem request)
        {
            try
            {
                var item = Db.GetById<Item>(request.Id);
                return item;
            }
            catch (Exception)
            {

                throw new HttpError(HttpStatusCode.NotFound, "Mabel's CMS", "Item {0} does not exist".Fmt(request.Id));
            }
           
        }
      
        public object Put(StoreItem item)
        {
            try
            {
                var date = item.LastWeighed.GetValueOrDefault();
                if (date.Year < 2008)
                    item.LastWeighed = null;
                Db.UpdateOnly(item, q => new { q.Name, q.Name_fr, q.Name_en, q.Description_en, q.Description_fr, q.IsActive, q.IsCoreItem, q.IsSale, q.PriceCAD, q.PriceUSD, q.MassGrams, q.LastWeighed, q.Groups, q.ApplyCoupon, q.Taxes }, q => q.id == item.id);
                return item;
            }
            catch (Exception webEx)
            {
                throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "Database error when trying to update an item:" + webEx.Message);
            }
        }

        public object Get(GetItemGroups request)
        {    
            var grupos = new List<string>(); var aux = new List<string>(); var result = new List<ReplaceItemGroupResponse>();
            var items = Db.Select<Item>();
            foreach (Item item in items)
                if (item.Groups!=null) {
                   aux = item.Groups.Split(',').ToList();
                   foreach(string group in aux)
                       grupos.AddIfNotExists<string>(group);
                }
            grupos.Sort();
            foreach(string group in grupos)
                result.Add(new ReplaceItemGroupResponse { Name = group});

            return result;
        } 

        public object Put(ReplaceItemGroup request)
        {
            try
            {
                Db.Update<Item>(set: "groups = REPLACE(groups,{0},{1})".Params(request.pk, request.value), where: "groups like '%{0}%'".Fmt(request.pk));
                return new ReplaceItemGroupResponse { Name = request.value };
            }
            catch (Exception webEx)
            {
                throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "Database error when trying to change name of category:" + webEx.Message);
            }
        }         

        public object Post(GetMatchingLayoutsOfItems request)
        {
            String lista = String.Join("','", request.itemsIds);
            var consulta =@"SELECT sb.id, sb.name,sb.isActive FROM `StdItems_StdTemplates` sisb
                                                    inner join 
                                                    Standards_BuilderTemplates sb on sisb.tmplid= sb.id  
                                                    where sisb.itemCode in ('"+lista+"')  order by sb.name";
            var result= Db.Select<Template>(consulta);
            return result;
        }

        public object Delete(DeleteItemGroup request)
        {
            using (IDbTransaction dbTrans = Db.OpenTransaction(IsolationLevel.RepeatableRead))
            {
                try
                {
                    Db.Update<Item>(set: "groups = REPLACE(groups,{0},{1})".Params(request.Name + ",", ""), where: "groups like '%{0}%'".Fmt(request.Name + ","));
                    Db.Update<Item>(set: "groups = REPLACE(groups,{0},{1})".Params("," + request.Name, ""), where: "groups like '%{0}%'".Fmt("," + request.Name));
                    Db.Update<Item>(set: "groups = REPLACE(groups,{0},{1})".Params(request.Name, ""), where: "groups like '%{0}%'".Fmt(request.Name));
                    Db.UpdateOnly(new Item { Groups = null }, item => item.Groups, item => item.Groups == "");
                     dbTrans.Commit();
                    return new DeleteItemGroupResponse();
                }
                catch (Exception webEx)
                {
                    dbTrans.Rollback();
                    throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "Database error when trying to change name of category:" + webEx.Message);
                }
            }

        }

        public object Get(GetItemMany2ManyTables request) {
            var result = new GetItemMany2ManyTablesResponse();
                result.item = Db.GetById<Item>(request.Id);
                
                var consulta = @"SELECT SIc.name as Name ,SIc.Code as Code
                                FROM Standards_Items SI  inner join `StdItems_StdIcons` StdI on SI.Code = StdI.itemCode 
                                                         inner join Standards_Icons SIc on StdI.iconCode=SIc.code 
                                                         where SI.id = '{0}'".Fmt(request.Id);
                result.icons = Db.Select<Icon>(consulta);

                consulta = @"SELECT SIc.name  as Name,SIc.Id as Id
                                FROM Standards_Items SI inner join `StdItems_StdColours` Stdc on SI.Code = Stdc.itemCode 
                                                        inner join Standards_Colours SIc on Stdc.colourId=SIc.Id 
                                                        where SIc.id!=0 and  SI.id = '{0}'".Fmt(request.Id);
                result.colours = Db.Select<Colour>(consulta);

                consulta = @"SELECT ST.name  as Name ,ST.Id as Id
                                FROM Standards_Items SI  inner join `StdItems_StdTemplates` StdT on SI.Code = StdT.itemCode 
                                                         inner join Standards_BuilderTemplates ST on StdT.tmplId=ST.Id 
                                                         where SI.id = '{0}'".Fmt(request.Id);
                result.templates = Db.Select<Template>(consulta);

            return result;
        }

        public object Get(GetItemMany2ManyIconsTable request)
        {
            var consulta = @"SELECT SIc.name as Name ,SIc.Code as Code
                                FROM Standards_Items SI  inner join `StdItems_StdIcons` StdI on SI.Code = StdI.itemCode 
                                                         inner join Standards_Icons SIc on StdI.iconCode=SIc.code 
                                                         where SI.id = '{0}'".Fmt(request.Id);
            return  Db.Select<Icon>(consulta);
        }

        public object Get(GetItemMany2ManyColoursTable request)
        {
            var consulta = @"SELECT SIc.name  as Name,SIc.Id as Id
                                FROM Standards_Items SI inner join `StdItems_StdColours` Stdc on SI.Code = Stdc.itemCode 
                                                        inner join Standards_Colours SIc on Stdc.colourId=SIc.Id 
                                                        where  SIc.id!=0 and SI.id = '{0}'".Fmt(request.Id);
           return Db.Select<Colour>(consulta);
        }

        public object Get(GetItemMany2ManyLayoutsTable request)
        {
            var consulta = @"SELECT ST.name  as Name ,ST.Id as Id
                                FROM Standards_Items SI  inner join `StdItems_StdTemplates` StdT on SI.Code = StdT.itemCode 
                                                         inner join Standards_BuilderTemplates ST on StdT.tmplId=ST.Id 
                                                         where SI.id = '{0}'".Fmt(request.Id);
            return Db.Select<Template>(consulta);
        }

        public object Put(Many2ManyItemUpdate itemTrans)
        {
            using (IDbTransaction dbTrans = Db.OpenTransaction(IsolationLevel.RepeatableRead))
            {
                try
                {
                    Item item = Db.GetById<Item>(itemTrans.Id);
                    item.Name = itemTrans.Name; 
                    item.Name_fr = itemTrans.Name_fr;
                    item.Description_en = itemTrans.Description_en;
                    item.Name_en = itemTrans.Name_en;
                    item.Description_fr = itemTrans.Description_fr;
                    item.IsActive = itemTrans.IsActive; 
                    item.IsCoreItem = itemTrans.IsCoreItem; 
                    item.IsSale = itemTrans.IsSale; 
                    item.ApplyCoupon = itemTrans.ApplyCoupon; 
                    item.PriceCAD = itemTrans.PriceCAD; 
                    item.PriceUSD = itemTrans.PriceUSD;
                    item.Groups = itemTrans.Groups;

                    Db.Delete<Item_Icons>(q => q.itemCode == item.Code);
                    Db.Delete<Item_Templates>(q => q.itemCode == item.Code);
                    Db.Delete<Item_Colours>(q => q.itemCode == item.Code);


                    Db.UpdateOnly(item, q => new { q.Name, q.Name_fr, q.Name_en, q.Description_en, q.Description_fr, q.IsActive, q.IsCoreItem, q.IsSale,q.ApplyCoupon, q.PriceCAD, q.PriceUSD, q.Groups }, q => q.id == itemTrans.Id);
                    if (!itemTrans.MultiSIcons.IsEmpty())
                    {
                        var newItemIcons = itemTrans.MultiSIcons.Select(iconCode => new Item_Icons { iconCode = iconCode, itemCode = item.Code }).ToList();
                        Db.InsertAll<Item_Icons>(newItemIcons);
                    }
                    if (!itemTrans.MultiSColours.IsEmpty())
                    {
                        var newItemColours = itemTrans.MultiSColours.Select(colourId => new Item_Colours { colourId = colourId, itemCode = item.Code }).ToList();
                        Db.InsertAll<Item_Colours>(newItemColours);
                    }
                    if (!itemTrans.MultiSLayouts.IsEmpty())
                    {
                        var newLayoutIcons = itemTrans.MultiSLayouts.Select(layoutId => new Item_Templates { itemCode = item.Code, tmplId = layoutId }).ToList();
                        Db.InsertAll<Item_Templates>(newLayoutIcons);
                    }

                    dbTrans.Commit();
                    return itemTrans;

                }// try

                catch (Exception webEx)
                {
                    dbTrans.Rollback();
                    throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "Transaction error when updating Many2Many or Item tables:" + webEx.Message);

                }
            }// transaction

        }
        
         // Icons Verbs

        public object Get(Icons request)
        {
            return Db.Select<Icon>();
        }

        public object Get(GetIcon request)
        {
            try
            {
                return Db.GetById<Icon>(request.Id);
            }
            catch (Exception)
            {

                throw new HttpError(HttpStatusCode.NotFound, "Mabel's CMS", "Icon {0} does not exist".Fmt(request.Id));
            }
        }

        public object Get(GetItemsByIcon request)
        {     
            return Db.Select<Item>("Select SI.* from Standards_Items SI inner join StdItems_StdIcons Std on SI.code = Std.itemCode and Std.iconCode=" + request.iconID);
         }

        public object Get(GetLayoutsByIcon request)
        {
            var consulta = @"SELECT sb.id, sb.name,sb.isActive FROM `StdIcons_StdTemplates` sisb
                                                    inner join 
                                                    Standards_BuilderTemplates sb on sisb.tmplid= sb.id  
                                                    where sisb.iconCode= " + request.iconID;
            return Db.Select<Template>(consulta);
        }
        

        public object Post(TransactionIcon iconTrans)
        {
           using (IDbTransaction dbTrans = Db.OpenTransaction(IsolationLevel.RepeatableRead))
            {
             try
               {    
                var newIcon = new Icon
                                       {
                                           id = iconTrans.Code,
                                           Name = iconTrans.Name,
                                           Name_en = iconTrans.Name_en,
                                           Name_fr = iconTrans.Name_fr,
                                           IsActive = iconTrans.IsActive,
                                           Categories = iconTrans.Categories
                                       };
                    Db.Save<Icon>(newIcon);


                    var newItemIcons = iconTrans.MultiSItems.Select(itemCode => new Item_Icons { iconCode = iconTrans.Code, itemCode = itemCode }).ToList();
                    Db.InsertAll<Item_Icons>(newItemIcons);

                    var newLayoutIcons =
                        iconTrans.MultiSLayouts.Select(
                            layoutId => new Templates_Icons { iconCode = iconTrans.Code, tmplId = layoutId }).ToList();
                    Db.InsertAll<Templates_Icons>(newLayoutIcons);

                    var newResourceList = new List<Resource>();
                    var newVariantList = new List<Variant>();
                    if (iconTrans.Bagtagc != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "BagTag", TextC = iconTrans.Bagtagc.ToInt(), TextM = iconTrans.Bagtagm.ToInt(), TextY = iconTrans.Bagtagy.ToInt(), TextK = iconTrans.Bagtagk.ToInt(), Variant = "" });
                    if (iconTrans.Binc != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "Bin", TextC = iconTrans.Binc.ToInt(), TextM = iconTrans.Binm.ToInt(), TextY = iconTrans.Biny.ToInt(), TextK = iconTrans.Bink.ToInt(), Variant = "" });
                    if (iconTrans.Bookc != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "Book", TextC = iconTrans.Bookc.ToInt(), TextM = iconTrans.Bookm.ToInt(), TextY = iconTrans.Booky.ToInt(), TextK = iconTrans.Bookk.ToInt(), Variant = "" });
                    if (iconTrans.Canisterc != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "Canister", TextC = iconTrans.Canisterc.ToInt(), TextM = iconTrans.Canisterm.ToInt(), TextY = iconTrans.Canistery.ToInt(), TextK = iconTrans.Canisterk.ToInt(), Variant = "" });
                    if (iconTrans.IronOnc != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "IronOn", TextC = iconTrans.IronOnc.ToInt(), TextM = iconTrans.IronOnm.ToInt(), TextY = iconTrans.IronOny.ToInt(), TextK = iconTrans.IronOnk.ToInt(), Variant = "" });
                    if (iconTrans.PSShoec != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "PSShoe", TextC = iconTrans.PSShoec.ToInt(), TextM = iconTrans.PSShoem.ToInt(), TextY = iconTrans.PSShoey.ToInt(), TextK = iconTrans.PSShoek.ToInt(), Variant = "" });
                    if (iconTrans.Shoec != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "Shoe", TextC = iconTrans.Shoec.ToInt(), TextM = iconTrans.Shoem.ToInt(), TextY = iconTrans.Shoey.ToInt(), TextK = iconTrans.Shoek.ToInt(), Variant = "" });
                    if (iconTrans.StickyAc != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "Sticky", TextC = iconTrans.StickyAc.ToInt(), TextM = iconTrans.StickyAm.ToInt(), TextY = iconTrans.StickyAy.ToInt(), TextK = iconTrans.StickyAk.ToInt(), Variant = "a" });
                    if (iconTrans.StickyBc != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "Sticky", TextC = iconTrans.StickyBc.ToInt(), TextM = iconTrans.StickyBm.ToInt(), TextY = iconTrans.StickyBy.ToInt(), TextK = iconTrans.StickyBk.ToInt(), Variant = "b" });
                    if (iconTrans.StickyCc != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "Sticky", TextC = iconTrans.StickyCc.ToInt(), TextM = iconTrans.StickyCm.ToInt(), TextY = iconTrans.StickyCy.ToInt(), TextK = iconTrans.StickyCk.ToInt(), Variant = "c" });
                    if (iconTrans.Spicec != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "Spice", TextC = iconTrans.Spicec.ToInt(), TextM = iconTrans.Spicem.ToInt(), TextY = iconTrans.Spicey.ToInt(), TextK = iconTrans.Spicek.ToInt(), Variant = "" });
                    if (iconTrans.SkinnyMinic != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "SkinnyMini", TextC = iconTrans.SkinnyMinic.ToInt(), TextM = iconTrans.SkinnyMinim.ToInt(), TextY = iconTrans.SkinnyMiniy.ToInt(), TextK = iconTrans.SkinnyMinik.ToInt(), Variant = "" });
                    if (iconTrans.TagMatec != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "TagMate", TextC = iconTrans.TagMatec.ToInt(), TextM = iconTrans.TagMatem.ToInt(), TextY = iconTrans.TagMatey.ToInt(), TextK = iconTrans.TagMatek.ToInt(), Variant = "" });
                    if (iconTrans.TeenyTagc != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "TeenyTag", TextC = iconTrans.TeenyTagc.ToInt(), TextM = iconTrans.TeenyTagm.ToInt(), TextY = iconTrans.TeenyTagy.ToInt(), TextK = iconTrans.TeenyTagk.ToInt(), Variant = "" });
                    if (iconTrans.Wristbandc != "")
                        newResourceList.Add(new Resource { Icon = iconTrans.Code, ResourceName = "Wristband", TextC = iconTrans.Wristbandc.ToInt(), TextM = iconTrans.Wristbandm.ToInt(), TextY = iconTrans.Wristbandy.ToInt(), TextK = iconTrans.Wristbandk.ToInt(), Variant = "" });

                    if (iconTrans.StickyChoice1 != 'N')
                        newVariantList.Add(new Variant { Icon = iconTrans.Code, Choice = 1, Variante = iconTrans.StickyChoice1 });
                    if (iconTrans.StickyChoice2 != 'N')
                        newVariantList.Add(new Variant { Icon = iconTrans.Code, Choice = 2, Variante = iconTrans.StickyChoice2 });
                    if (iconTrans.StickyChoice3 != 'N')
                        newVariantList.Add(new Variant { Icon = iconTrans.Code, Choice = 3, Variante = iconTrans.StickyChoice3 });

                    Db.InsertAll(newResourceList);
                    Db.InsertAll(newVariantList);

                    dbTrans.Commit();
             
            }// try
            
            catch (Exception webEx)
            {
                dbTrans.Rollback();
                throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "Transaction error when inserting new icon:" + webEx.Message);

            }
          }// transaction
        return iconTrans;
       }

        public object Put(TransactionIcon iconTrans)
        {
            using (IDbTransaction dbTrans = Db.OpenTransaction(IsolationLevel.RepeatableRead))
            {
                try
                {        
                    
                    Db.Delete<Item_Icons>(q => q.iconCode == iconTrans.Code);
                    Db.Delete<Templates_Icons>(q => q.iconCode == iconTrans.Code);
                    Db.Delete<Resource>(q => q.Icon == iconTrans.Code);
                    Db.Delete<Variant>(q => q.Icon == iconTrans.Code);
                    dbTrans.Commit();

                }// try

                catch (Exception webEx)
                {
                    // dbTrans.Rollback();
                    throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "Transaction error when updating the icon:" + webEx.Message);

                }
            }// transaction
            return Post(iconTrans);
        }


        public object Get(GetIconInfo request)
        {
            var result = new GetIconInfoResponse();
            try
            {
                // Standards_Icons
                result.Icon = Db.GetById<Icon>(request.Id);
                // StdItems_StdIcons
                result.ItemIcons = Db.Select<Item_Icons>(items => items.iconCode == request.Id);
                // StdTmpl_StdIcons
                result.TemplatesIcons = Db.Select<Templates_Icons>(tmpl => tmpl.iconCode == request.Id);
                // Standards_Resources
                result.Resources = Db.Select<Resource>(resource => resource.Icon == request.Id);
                // Standards_Variants
                result.Variants = Db.Select<Variant>(variant => variant.Icon == request.Id);
                return result;
            } 

            catch (Exception webEx)
            {
                throw new HttpError(HttpStatusCode.NotFound, "Mabel's CMS", "Icon {0} does not exist".Fmt(request.Id));

            }
        }
         
        public object Any(SetIconToItems iconReq)
        {
           try
           {
            Db.Delete<Item_Icons>(p => p.iconCode == iconReq.iconID);
            var toInsert = iconReq.itemIds.Select(itemCode => new Item_Icons { iconCode = iconReq.iconID, itemCode = itemCode }).ToList();            
            Db.InsertAll<Item_Icons>(toInsert);
            }
            catch (Exception webEx)
            {
                throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "Database error when trying to assign icon {0} to items:".Fmt(iconReq.iconID)+ webEx.Message);
            }
            return iconReq;
        }

        public object Any(SetIconToLayouts iconReq)
        {
            try {
                var toInsert = iconReq.templIds.Select(templId => new Templates_Icons { iconCode = iconReq.iconID, tmplId = templId }).ToList();    
                Db.Delete<Templates_Icons>(p => p.iconCode == iconReq.iconID);               
                Db.InsertAll<Templates_Icons>(toInsert);
                return iconReq;
            }
            catch (Exception webEx)
            {
                throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "Database error when trying to assign icon {0} to layouts:".Fmt(iconReq.iconID) + webEx.Message);
            }
        }

        public object Get(GetIconMany2ManyTables request)
        {
            try {
            var result = new GetIconMany2ManyTablesResponse();
                result.icon = Db.GetById<Icon>(request.Id);

                var consulta = @"SELECT SI.code  as Code ,SI.Id as Id
                                 FROM Standards_Items SI  inner join `StdItems_StdIcons` StdI on SI.Code = StdI.itemCode                                                          
                                                         where StdI.iconcode = '{0}'".Fmt(request.Id);
                result.items = Db.Select<Item>(consulta);


                consulta = @"SELECT ST.name  as Name ,ST.Id as Id
                                FROM `StdIcons_StdTemplates` StdT  
                                                         inner join Standards_BuilderTemplates ST on StdT.tmplId=ST.Id 
                                                         where StdT.iconCode  = '{0}'".Fmt(request.Id);

                result.templates = Db.Select<Template>(consulta);

            return result;
            }
            catch (Exception webEx)
            {
                throw new HttpError(HttpStatusCode.NotFound, "Mabel's CMS", "Icon {0} does not exist".Fmt(request.Id));
            }
        }

        public object Get(GetIconMany2ManyLayoutsTable request)
        {
            var consulta = @"SELECT ST.name  as Name ,ST.Id as Id
                                FROM `StdIcons_StdTemplates` StdT  
                                                         inner join Standards_BuilderTemplates ST on StdT.tmplId=ST.Id 
                                                         where StdT.iconCode  = '{0}'".Fmt(request.Id);
            return Db.Select<Template>(consulta);
        }


        public object Get(GetIconMany2ManyItemsTable request)
        {
            var consulta = @"SELECT SI.code  as Code ,SI.Id as Id
                                 FROM Standards_Items SI  inner join `StdItems_StdIcons` StdI on SI.Code = StdI.itemCode                                                          
                                                         where StdI.iconcode = '{0}'".Fmt(request.Id);
           return Db.Select<Item>(consulta);
        }


        public object Put(Many2ManyIconUpdate iconTrans)
        {
            using (IDbTransaction dbTrans = Db.OpenTransaction(IsolationLevel.RepeatableRead))
            {
                try
                {
                    Icon icon = new Icon
                    {
                        id =   iconTrans.Id,
                        Name = iconTrans.Name,
                        Name_en = iconTrans.Name_en,
                        Name_fr = iconTrans.Name_fr,
                        IsActive = iconTrans.IsActive
                    };

                    Db.Delete<Item_Icons>(q => q.iconCode == icon.id);
                    Db.Delete<Templates_Icons>(q => q.iconCode == icon.id);


                    Db.UpdateOnly(icon, q => new { q.Name, q.Name_fr, q.Name_en, q.IsActive }, q => q.id == icon.id);
                    if (!iconTrans.MultiSItems.IsEmpty())
                    {
                        var newItemIcons = iconTrans.MultiSItems.Select(itemCode => new Item_Icons { itemCode = itemCode, iconCode = icon.id }).ToList();
                        Db.InsertAll<Item_Icons>(newItemIcons);
                    }
                    if (!iconTrans.MultiSLayouts.IsEmpty())
                    {
                        var newLayoutIcons = iconTrans.MultiSLayouts.Select(layoutId => new Templates_Icons { iconCode = icon.id, tmplId = layoutId }).ToList();
                        Db.InsertAll<Templates_Icons>(newLayoutIcons);
                    }

                    dbTrans.Commit();
                    return iconTrans;

                }// try

                catch (Exception webEx)
                {
                    dbTrans.Rollback();
                    throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "Transaction error when updating Many2Many or Icon tables:" + webEx.Message);

                }
            }// transaction

        }
        
        // Resources verbs               
        
        public object Post(StoreResource resource)
        {
            Db.Insert(resource);
            resource.id = (int)Db.GetLastInsertId();
            return resource;
        }

        public object Put(StoreResource resource)
        {
            Db.UpdateOnly(resource, q => new { q.TextC, q.TextM, q.TextY, q.TextK }, q => q.id == resource.id);
            return resource;
        }

        // Layout's verbs

        public object Get(GetAllLayouts request)
        {
            var result = Db.Select<Template>();
            return result;
        }


        public object Get(GetLayout request)
        {
            try
            {
                var response = new GetLayoutResponse();
                
                response.template = Db.GetById<Template>(request.Id);

                var boxes = Db.Select<Layout_Box>(q => q.BuilderTemplateId == request.Id);
                response.boxes = new List<Layout_Box_Container>();
                foreach (var box in boxes){                    
                    var box_sets = Db.Select<Layout_Set>(q => q.BoxId == box.id);
                    var linkedSets = new List<Layout_Set_Container>();
                    if (box_sets != null)
                    {                        
                        foreach (var box_set in box_sets)
                        {                            
                            var box_seticons = Db.Select<Layout_SetIcon>(q => q.id == box_set.id);
                            var auxSet = new Layout_Set_Container(box_set,box_seticons);
                            linkedSets.Add(auxSet);
                        }                        
                    }
                    var linkedBox = new Layout_Box_Container(box, linkedSets);
                    response.boxes.Add(linkedBox);
                }
                return response;  
            }
            catch (Exception web)
            {

                throw new HttpError(HttpStatusCode.NotFound, "Mabel's CMS", "Layout {0} does not exist".Fmt(request.Id));
            }
                
        }

        public object Put(UpdateLayout layout)
        {
            using (IDbTransaction dbTrans = Db.OpenTransaction(IsolationLevel.RepeatableRead))
            {
                try
                {
                   /* Db.Save<Template>(layout.template);
                    if (layout.boxes!=null)
                    Db.SaveAll<Layout_Box>(layout.boxes);
                    if (layout.sets != null)
                    Db.SaveAll<Layout_Set>(layout.sets);
                    if (layout.seticons != null)
                    Db.SaveAll<Layout_SetIcon>(layout.seticons);*/
                }// try

                catch (Exception webEx)
                {
                    // dbTrans.Rollback();
                    throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "Transaction error when inserting new icon:" + webEx.Message);

                }
            }// transaction
            return layout;
        }

        public object Get(GetLayoutMany2ManyTables request)
        {
            var result = new GetLayoutMany2ManyTablesResponse();
                result.temp = Db.GetById<Template>(request.Id);

                var consulta = @"SELECT SI.code  as Code ,SI.Name as Name
                                FROM Standards_Icons SI  inner join `StdIcons_StdTemplates` StdT on SI.Code = StdT.iconCode 
                                                         inner join Standards_BuilderTemplates ST on StdT.tmplId=ST.Id 
                                                         where ST.id =  '{0}'".Fmt(request.Id);
                result.icons = Db.Select<Icon>(consulta);

                consulta = @"SELECT SC.name  as Name ,SC.Id as Id
                                FROM Standards_Colours SC  inner join `StdColours_StdTemplates` StdT on SC.Id = StdT.colourId 
                                                         inner join Standards_BuilderTemplates ST on StdT.tmplId=ST.Id 
                                                         where SC.id!=0 and ST.id = '{0}'".Fmt(request.Id);
                result.colours = Db.Select<Colour>(consulta);

                consulta = @"SELECT SI.code  as Code ,SI.Id as Id
                                FROM Standards_Items SI  inner join `StdItems_StdTemplates` StdT on SI.Code = StdT.itemCode 
                                                         inner join Standards_BuilderTemplates ST on StdT.tmplId=ST.Id 
                                                         where ST.id = '{0}'".Fmt(request.Id);
                result.items = Db.Select<Item>(consulta);

            return result;
        }

        public object Get(GetLayoutMany2ManyIconsTable request)
        {
            var consulta = @"SELECT SI.code  as Code ,SI.Name as Name
                                FROM Standards_Icons SI  inner join `StdIcons_StdTemplates` StdT on SI.Code = StdT.iconCode 
                                                         inner join Standards_BuilderTemplates ST on StdT.tmplId=ST.Id 
                                                         where ST.id =  '{0}'".Fmt(request.Id);
            return Db.Select<Icon>(consulta);
        }

        public object Get(GetLayoutMany2ManyColoursTable request)
        {
            var consulta = @"SELECT SC.name  as Name ,SC.Id as Id
                                FROM Standards_Colours SC  inner join `StdColours_StdTemplates` StdT on SC.Id = StdT.colourId 
                                                         inner join Standards_BuilderTemplates ST on StdT.tmplId=ST.Id 
                                                         where SC.id!=0 and  ST.id = '{0}'".Fmt(request.Id);
            return Db.Select<Colour>(consulta);
        }

        public object Get(GetLayoutMany2ManyItemsTable request)
        {
            var consulta = @"SELECT SI.code  as Code ,SI.Id as Id
                                FROM Standards_Items SI  inner join `StdItems_StdTemplates` StdT on SI.Code = StdT.itemCode 
                                                         inner join Standards_BuilderTemplates ST on StdT.tmplId=ST.Id 
                                                         where ST.id = '{0}'".Fmt(request.Id);
            return Db.Select<Item>(consulta);
        }


        public object Put(Many2ManyLayoutUpdate tempTrans)
        {
            using (IDbTransaction dbTrans = Db.OpenTransaction(IsolationLevel.RepeatableRead))
            {
                try
                {
                    Template temp = new Template
                    {
                        Name = tempTrans.Name,
                        PrinterMaterial = tempTrans.PrinterMaterial,
                        Score = tempTrans.Score,
                        IsActive = tempTrans.IsActive
                    };

                    Db.Delete<Item_Templates>(q => q.tmplId == tempTrans.Id);
                    Db.Delete<Templates_Colours>(q => q.tmplId == tempTrans.Id);
                    Db.Delete<Templates_Icons>(q => q.tmplId == tempTrans.Id);


                    Db.UpdateOnly(temp, q => new { q.Name, q.IsActive, q.Score, q.PrinterMaterial }, q => q.Id == tempTrans.Id);
                    if (!tempTrans.MultiSItems.IsEmpty())
                    {
                        var newItemTemp = tempTrans.MultiSItems.Select(itemCode => new Item_Templates { itemCode = itemCode, tmplId = tempTrans.Id }).ToList();
                        Db.InsertAll<Item_Templates>(newItemTemp);
                    }
                    if (!tempTrans.MultiSColours.IsEmpty())
                    {
                        var newColourTemp = tempTrans.MultiSColours.Select(colourId => new Templates_Colours { colourId = colourId, tmplId = tempTrans.Id }).ToList();
                        Db.InsertAll<Templates_Colours>(newColourTemp);
                    }
                    if (!tempTrans.MultiSIcons.IsEmpty())
                    {
                        var newColourTemp = tempTrans.MultiSIcons.Select(iconCode => new Templates_Icons { iconCode = iconCode, tmplId = tempTrans.Id }).ToList();
                        Db.InsertAll<Templates_Icons>(newColourTemp);
                    }

                    dbTrans.Commit();
                    return tempTrans;

                }// try

                catch (Exception webEx)
                {
                    dbTrans.Rollback();
                    throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "Transaction error when updating the icon:" + webEx.Message);

                }
            }// transaction

        }

      // Content verbs

        public object Get(GetContent content)
        {            
                try
                {
                   return Db.GetById<Content>(content.Id); 
                }

                catch (Exception webEx)
                {                   
                    throw new HttpError(HttpStatusCode.InternalServerError, "Mabel's CMS", "No such website content:" .Fmt(content.Id));
                }
           
            
        }
        
        

         public object Put (SetContent content)
        {
            Db.UpdateOnly(content, q => new { q.Value }, q => q.id == content.id);
            return content;
        }

        

     }
}
