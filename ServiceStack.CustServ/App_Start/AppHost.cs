using System;
using System.Data;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using ServiceStack.CacheAccess;
using ServiceStack.CacheAccess.Providers;
using ServiceStack.Configuration;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.MySql;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.CustServ.Models.Authentication;
using ServiceStack.WebHost.Endpoints;

[assembly: WebActivator.PreApplicationStartMethod(typeof(ServiceStack.CustServ.App_Start.AppHost), "Start")]

namespace ServiceStack.CustServ.App_Start
{
	public class AppHost: AppHostBase
	{		
		public AppHost() //Tell ServiceStack the name and where to find your web services
            : base("MisMabel internal DB interface", typeof(DBWebService).Assembly) { }

		public override void Configure(Funq.Container container)
		{
			//Set JSON web services to return idiomatic JSON camelCase properties
			ServiceStack.Text.JsConfig.EmitCamelCaseNames = true;
		
			
			//Uncomment to change the default ServiceStack configuration
			SetConfig(new EndpointHostConfig {
                EnableFeatures= ServiceStack.ServiceHost.Feature.Json |ServiceStack.ServiceHost.Feature.Metadata,
                DebugMode = true
		});

			//Enable Authentication
			//ConfigureAuth(container);
            
			//Register all your dependencies
            OrmLiteConfig.DialectProvider = MySqlDialectProvider.Instance;
            var connectionString = ConfigurationManager.ConnectionStrings["production"].ConnectionString;
            container.Register<IDbConnectionFactory>(new OrmLiteConnectionFactory(connectionString, OrmLiteConfig.DialectProvider));
            ConfigureAuth(container);
		}

		// Uncomment to enable ServiceStack Authentication and CustomUserSession
        private void ConfigureAuth(Funq.Container container)
		{
			var appSettings = new AppSettings();

			//Default route: /auth/{provider}
            Plugins.Add(new AuthFeature(() => new AuthUserSession(),
				new IAuthProvider[] {
					new CredentialsAuthProvider(appSettings)
				}));
            container.Register<ICacheClient>(new MemoryCacheClient());
			//Default route: /register
			Plugins.Add(new RegistrationFeature()); 
            // registration goes to the database
			container.Register<IUserAuthRepository>(c =>new OrmLiteAuthRepository(c.Resolve<IDbConnectionFactory>()));
            /* Create the user tables for registration
			    var authRepo = (OrmLiteAuthRepository)container.Resolve<IUserAuthRepository>();
			    authRepo.CreateMissingTables();
             */
        }		

		public static void Start()
		{
			new AppHost().Init();
		}
	}
}
