/*global Backbone */
var app = app || {};
var ENTER_KEY = 13;

(function ($) {
	'use strict';    
               app = {
                    view: function(name){
                       return this.layout.views[name];
                    },
                    // Create this closure to contain the cached modules
                    module: function () {
                        // Internal module cache.
                        var modules = {};

                        // Create a new module reference scaffold or load an
                        // existing module.
                        return function (name) {
                            // If this module has already been created, return it.
                            if (modules[name]) {
                                return modules[name];
                            }

                            // Create a module and save it under this name
                            return modules[name] = { Views: {} };
                        };
                    } ()
                };
                Backbone.Layout.configure(
                  {  manage: true ,
                     cacheTemplates:{},
                     fetchTemplate: function (path) {
                       var done = this.async();
                         return this.cacheTemplates[path] = this.cacheTemplates[path] ||
                             $.get(path , function(contents) {
                                 done(contents);
                             }, "text");
				    },
                    renderTemplate: function(template, context) {
                        var result = Handlebars.compile(template)(context);
                       return result;
                    }               
                });  
                
                Handlebars.registerHelper('select', function( value, options ){
                    var $el = $('<select />').html( options.fn(this) );
                    $el.find('[value=' + value + ']').attr({'selected':'selected'});
                    return $el.html();
                });

                Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
                    switch (operator) {
                        case '==':
                            return (v1 == v2) ? options.fn(this) : options.inverse(this);
                        case '===':
                            return (v1 === v2) ? options.fn(this) : options.inverse(this);
                        case '<':
                            return (v1 < v2) ? options.fn(this) : options.inverse(this);
                        case '<=':
                            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                        case '>':
                            return (v1 > v2) ? options.fn(this) : options.inverse(this);
                        case '>=':
                            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                        case '&&':
                            return (v1 && v2) ? options.fn(this) : options.inverse(this);
                        case '||':
                            return (v1 || v2) ? options.fn(this) : options.inverse(this);
                        default:
                            return options.inverse(this);
                    }
                });

                $.fn.serializeObject = function()
                    {
                        var o = {};
                        var a = this.serializeArray();
                        $.each(a, function() {
                            if (o[this.name] !== undefined) {
                                if (!o[this.name].push) {
                                    o[this.name] = [o[this.name]];
                                }
                                o[this.name].push(this.value || '');
                            } else {
                                o[this.name] = this.value || '';
                            }
                        });
                        return o;
                    };   
                    
                // Create a new main layout   
                  app.LoginLayout =  Backbone.Layout.extend({                    
                    el:"body",                                      
                    afterRender:function(){                           
			              $("body").addClass("login").removeClass('page-header-fixed');   
							Login.init();
                           }
                 });                           
			   // Create a new main layout   
                  app.MainLayout =  Backbone.Layout.extend({
                    prefix: "templates/",
                    template: "layout.html",   
                    el:"body",
                     events :{                       
                      "click #logout"  : "logout"
                    }, 
                    logout: function(){
                        var options={};
                        options.contentType = 'application/json';
                        options.type = 'post';
                        options.url = 'auth/logout/';
                        self = this;
                        options.success= this.logoutSuccess; 
                        this.user.fetch(options);    
                    },
                    logoutSuccess: function(user) {
			            user.signOut();
                        app.router.navigate('home', true);
		             }, 
                    serialize:function(){
                       return {user:this.user.toJSON()};
                     },                   
                    afterRender:function(){App.init();
                       $("body").removeClass("login").addClass('page-header-fixed');
                    }
                 });                
               
})(jQuery);
