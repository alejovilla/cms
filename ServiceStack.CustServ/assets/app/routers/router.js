/*global Backbone */
var app = app || {};

(function () {
	'use strict';

	var Router = Backbone.Router.extend({
		routes: {
            ""           : "home",            
            "home"       : "home",
            "login"      : "login",
			"items/update" : "itemupdate",
            "items/categories" : "categoriesupdate",
            "items/:id"  : "itemDetails",
            "icons/update" : "iconupdate",
            "icons/create" : "iconcreate",
            "layouts/update": "layoutupdate",
            "content/update": "contentupdate",
            "many2many/items": "many2manyitems",
            "many2many/layouts": "many2manylayouts",
            "many2many/icons": "many2manyicons",
            "many2many/colours": "many2manycolours"
		},        
        home: function(){
           // Authenticated user move to home page
            if(app.currentUser.get('isAuthenticated')){
                if (app.currentUser.hasPermissions(['items_update']))   
                  this.navigate('#items/update', true);
                else if (app.currentUser.hasPermissions(['icons_create']))
                           this.navigate('#icons/create', true);
            }
            // Unauthed user move to login page
            else{
                // app.layoutLogin.removeView(""); 
                 this.navigate('#login', true);  
                }                
        },
        login: function(){          
         app.layoutLogin = new app.LoginLayout();
         app.layoutLogin.setView("",new app.LoginView({model:app.currentUser}));
         app.layoutLogin.$el.appendTo("body");
         app.layoutLogin.render();
        },                
		itemupdate: function () {  
          app.currentUser = app.currentUser || new app.UserModel(); 
          if(app.currentUser.isSignedIn()){
               app.currentUser.getProfile().success(function(){
                   app.layout = app.layout || new app.MainLayout({user:app.currentUser});                    
                   if (app.currentUser.hasPermissions(['items_update'])){
                          app.Items.fetch().success(function(){      
                          app.layout.setView(".page-sidebar",new app.SidebarView({user:app.currentUser}));                    
                          var subview = new app.ItemCollectionView({collection: app.Items});
                          app.layout.setView(".page-content-body",new app.ItemWorkspaceView({operation:"Update Item", item_listView : subview})); 
                          app.layout.setView("#item_list",subview);
                          //app.layout.$el.appendTo("body");
                          app.layout.render();          
                          });
                   }
                   else {
                        app.layout.setView(".page-content-body",new app.NoAccessView());
                        app.layout.render(); 
                   }
               });
            }
            else {
                   this.navigate('#login', true);  
                 }                         
         },
         itemDetails: function(itemId){
           app.currentUser = app.currentUser || new app.UserModel(); 
           if(app.currentUser.isSignedIn()){
                app.currentUser.getProfile().success(function(){
                  app.layout = app.layout || new app.MainLayout({user:app.currentUser});
                   if (app.currentUser.hasPermissions(['items_update'])){
                        var itemSelected = app.Items.findWhere({id:parseInt(itemId)});
                        var dtlview = app.layout.setView("#item_detail",new app.ItemView({model:itemSelected}));
                        dtlview.$el.appendTo("#item_detail");
                        dtlview.render();
                        var itemBox = $('.portlet.box.yellow');
                        App.scrollTo(itemBox, -200); 
                     }
                   else {
                        app.layout.setView(".page-content-body",new app.NoAccessView());
                        app.layout.render(); 
                   }
               });
            }
            else {
                   this.navigate('#login', true);  
                 }                         
         },
        categoriesupdate: function(){
            app.currentUser = app.currentUser || new app.UserModel(); 
             if(app.currentUser.isSignedIn()){
                app.currentUser.getProfile().success(function(){
                   app.layout = app.layout || new app.MainLayout({user:app.currentUser});
                   if (app.currentUser.hasPermissions(['items_categories'])){
                         app.Groups.fetch().success(function(){
                              var subview = new app.CategoryCollectionView({collection: app.Groups});
                              app.layout.setView(".page-content-body",new app.CategoryWorkspaceView({operation:"Rename categories", cat_listView : subview})); 
                              app.layout.setView("#cat_list",subview);
                              app.layout.render();        
                          });
                   }
                   else {
                        app.layout.setView(".page-content-body",new app.NoAccessView());
                        app.layout.render(); 
                   }
               });
            }
            else {
                   this.navigate('#login', true);  
                 } 
        },
        iconcreate: function(){
            app.currentUser = app.currentUser || new app.UserModel(); 
            if(app.currentUser.isSignedIn()){
                app.currentUser.getProfile().success(function(){
                   app.layout = app.layout || new app.MainLayout({user:app.currentUser});
                   if (app.currentUser.hasPermissions(['icons_create'])){    
                         app.Items.fetch().success(function(){
                         app.Icons.fetch().success(function(){
                              app.layout.setView(".page-sidebar",new app.SidebarView({user:app.currentUser}));
                              var subview = new app.LayoutCollectionView({collection:new app.LayoutList(),user:app.currentUser});
                              app.layout.setView("#layout_list",subview);
                              app.layout.setView(".page-content-body",new app.IconInsertWorkspaceView({operation:"Insert new icon", itemCollection:app.Items, iconCollection:app.Icons, layoutView : subview,user:app.currentUser}));  
                              app.layout.render();
                   
                          });     
                      });
                   }
                   else {
                        app.layout.setView(".page-content-body",new app.NoAccessView());
                        app.layout.render(); 
                   }
               });
            }
            else {
                   this.navigate('#login', true);  
                 }                         
         },
        iconupdate: function () {				   
                app.currentUser = app.currentUser || new app.UserModel(); 
            if(app.currentUser.isSignedIn()){
                app.currentUser.getProfile().success(function(){
                   app.layout = app.layout || new app.MainLayout({user:app.currentUser});
                   if (app.currentUser.hasPermissions(['icons_update'])){    
                         app.Items.fetch().success(function(){
                         app.Icons.fetch().success(function(){
                         app.Layouts.fetch().success(function(){
                              app.layout.setView(".page-sidebar",new app.SidebarView({user:app.currentUser}));
                              app.layout.setView(".page-content-body",new app.IconUpdateWorkspaceView({operation:"Update existing icon",  itemCollection:app.Items, layoutCollection:app.Layouts, iconCollection:app.Icons, user:app.currentUser}));  
                              app.layout.render();
                   
                          });     
                          });
                          });
                   }
                   else {
                        app.layout.setView(".page-content-body",new app.NoAccessView());
                        app.layout.render(); 
                   }
               });
            }
            else {
                   this.navigate('#login', true);  
                 }                         
         },
        layoutupdate: function () {				   
             app.currentUser = app.currentUser || new app.UserModel(); 
             if(app.currentUser.isSignedIn()){
                app.currentUser.getProfile().success(function(){
                   app.layout = app.layout || new app.MainLayout({user:app.currentUser});
                   if (app.currentUser.hasPermissions(['layouts_update'])){
                         app.Layouts.fetch().success(function(){
                              app.layout.setView(".page-sidebar",new app.SidebarView({user:app.currentUser}));
                              app.layout.setView(".page-content-body",new app.LayoutWorkspaceView({operation:"Update existing layout",  layoutCollection:app.Layouts,  user:app.currentUser}));  
                              app.layout.render();
                          });
                   }
                   else {
                        app.layout.setView(".page-content-body",new app.NoAccessView());
                        app.layout.render(); 
                   }
               });
            }
            else {
                   this.navigate('#login', true);  
                 }                         
         },
        contentupdate: function () {				   
             app.currentUser = app.currentUser || new app.UserModel(); 
             if(app.currentUser.isSignedIn()){
                app.currentUser.getProfile().success(function(){
                   app.layout = app.layout || new app.MainLayout({user:app.currentUser});
                   if (app.currentUser.hasPermissions(['content_update'])){
                         app.orderConfEmailEn1 = new app.ContentModel({id:"OrderConfEmailEn1"});
                         app.orderConfEmailFr1 = new app.ContentModel({id:"OrderConfEmailFr1"});
                         app.orderConfEmailEn2 = new app.ContentModel({id:"OrderConfEmailEn2"});
                         app.orderConfEmailFr2 = new app.ContentModel({id:"OrderConfEmailFr2"});
                         app.orderConfEmailFr1.fetch().success(function(){
                         app.orderConfEmailEn1.fetch().success(function(){
                         app.orderConfEmailFr2.fetch().success(function(){
                         app.orderConfEmailEn2.fetch().success(function(){
                              app.layout.setView(".page-sidebar",new app.SidebarView({user:app.currentUser}));
                              app.layout.setView(".page-content-body",new app.ContentOCEWorkspaceView({operation:"Update Order confirmation email content", orderConfEmailFr1: app.orderConfEmailFr1, orderConfEmailEn1:app.orderConfEmailEn1,orderConfEmailFr2: app.orderConfEmailFr2, orderConfEmailEn2:app.orderConfEmailEn2, user:app.currentUser}));  
                              app.layout.render();
                          });
                          });
                          });
                          });
                   }
                   else {
                        app.layout.setView(".page-content-body",new app.NoAccessView());
                        app.layout.render(); 
                   }
               });
            }
            else {
                   this.navigate('#login', true);  
                 }                         
         },
         many2manyitems:function () {				   
             app.currentUser = app.currentUser || new app.UserModel(); 
             if(app.currentUser.isSignedIn()){
                app.currentUser.getProfile().success(function(){
                   app.layout = app.layout || new app.MainLayout({user:app.currentUser});
                   if (app.currentUser.hasPermissions(['many2many_items'])){                         
                         app.Items.fetch().success(function(){
                         app.Colours.fetch().success(function(){
                         app.Icons.fetch().success(function(){
                         app.Layouts.fetch().success(function(){                         
                              app.layout.setView(".page-sidebar",new app.SidebarView({user:app.currentUser}));
                              app.layout.setView(".page-content-body",new app.StdItems_workplaceView({operation:"Manage table content", items:app.Items, icons:app.Icons,templates:app.Layouts,colours:app.Colours,user:app.currentUser}));  
                              app.layout.render();                       
                          });
                          });
                          });
                          });
                   }
                   else {
                        app.layout.setView(".page-content-body",new app.NoAccessView());
                        app.layout.render(); 
                   }
               });
            }
            else {
                   this.navigate('#login', true);  
                 }                         
         },
         many2manylayouts:function () {				   
             app.currentUser = app.currentUser || new app.UserModel(); 
             if(app.currentUser.isSignedIn()){
                app.currentUser.getProfile().success(function(){
                   app.layout = app.layout || new app.MainLayout({user:app.currentUser});
                   if (app.currentUser.hasPermissions(['many2many_layouts'])){                         
                         app.Items.fetch().success(function(){
                         app.Colours.fetch().success(function(){
                         app.Icons.fetch().success(function(){
                         app.Layouts.fetch().success(function(){                         
                              app.layout.setView(".page-sidebar",new app.SidebarView({user:app.currentUser}));
                              app.layout.setView(".page-content-body",new app.StdBuilderTemplates_workplaceView({operation:"Manage table content", items:app.Items, icons:app.Icons,templates:app.Layouts,colours:app.Colours,user:app.currentUser}));  
                              app.layout.render();                       
                          });
                          });
                          });
                          });
                   }
                   else {
                        app.layout.setView(".page-content-body",new app.NoAccessView());
                        app.layout.render(); 
                   }
               });
            }
            else {
                   this.navigate('#login', true);  
                 }                         
         },
          many2manyicons:function () {				   
             app.currentUser = app.currentUser || new app.UserModel(); 
             if(app.currentUser.isSignedIn()){
                app.currentUser.getProfile().success(function(){
                   app.layout = app.layout || new app.MainLayout({user:app.currentUser});
                   if (app.currentUser.hasPermissions(['many2many_icons'])){                         
                         app.Items.fetch().success(function(){              
                         app.Icons.fetch().success(function(){
                         app.Layouts.fetch().success(function(){                         
                              app.layout.setView(".page-sidebar",new app.SidebarView({user:app.currentUser}));
                              app.layout.setView(".page-content-body",new app.StdIcons_workplaceView({operation:"Manage table content", items:app.Items, icons:app.Icons,templates:app.Layouts,user:app.currentUser}));  
                              app.layout.render();                       
                          });
                          });
                          });
                   }
                   else {
                        app.layout.setView(".page-content-body",new app.NoAccessView());
                        app.layout.render(); 
                   }
               });
            }
            else {
                   this.navigate('#login', true);  
                 }                         
         },
         many2manycolours:function () {				   
             app.currentUser = app.currentUser || new app.UserModel(); 
             if(app.currentUser.isSignedIn()){
                app.currentUser.getProfile().success(function(){
                   app.layout = app.layout || new app.MainLayout({user:app.currentUser});
                   if (app.currentUser.hasPermissions(['many2many_colours'])){                         
                         app.Items.fetch().success(function(){              
                         app.Colours.fetch().success(function(){
                         app.Layouts.fetch().success(function(){                         
                              app.layout.setView(".page-sidebar",new app.SidebarView({user:app.currentUser}));
                              app.layout.setView(".page-content-body",new app.StdColours_workplaceView({operation:"Manage table content", items:app.Items, colours:app.Colours,templates:app.Layouts,user:app.currentUser}));  
                              app.layout.render();                       
                          });
                          });
                          });
                   }
                   else {
                        app.layout.setView(".page-content-body",new app.NoAccessView());
                        app.layout.render(); 
                   }
               });
            }
            else {
                   this.navigate('#login', true);  
                 }                         
         }
         
    });
    app.currentUser = new app.UserModel();
	app.router = new Router();
	Backbone.history.start();
})();
