/*global Backbone */
var app = app || {};

(function () {
	'use strict';
           
            app.LayoutSetIconModel = Backbone.Model.extend({                
                defaults: {
                     iconCode: 0                                      
                }                
            });
})();
