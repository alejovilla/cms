/*global Backbone */
var app = app || {};

(function () {
	'use strict';
           
            app.LayoutBoxModel = Backbone.Model.extend({                
                defaults: {
                     BuilderTemplateId : 0,
                     boxName    : ""  ,
                     top : 0, 
                     left :0 ,
                     bottom :0,
                     right :0,
                     adjustLeading  :0,        
                     squareBox :false                   
                }
            });
})();
