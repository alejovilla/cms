/*global Backbone */
var app = app || {};

(function () {
	'use strict';
     var coloursUrl = "colours";
 // Colours 
             app.ColourModel = Backbone.Model.extend({
                defaults: {
                      name_fr : "",
                      name_en : "",
                      isActive : 0                     
                },
                initialize: function () {                    
                },                
                url: function () {
                     return this.get("id") ? coloursUrl + '/' + this.get("id") : coloursUrl;
                },
                clear: function () {
                    this.destroy();
                    this.view.remove();
                }

            });
})();
