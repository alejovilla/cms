/*global Backbone */
var app = app || {};

(function () {
	'use strict';
            var  itemsUrl = "items";
            app.ItemModel = Backbone.Model.extend({
                // Default attributes for the todo.
                defaults: {                    
                      code: "",
                      name    : "",
                      name_fr : "",
                      name_en : "",
                      isActive : true,
                      isCoreItem : false,
                      type          : "",
                      colourChoices : "",
                      iconChoicesRegex : "",
                      groups        : ""
                },
                initialize: function () {                    
                },               
                validate: function(attrs, options) {
                    if ((attrs.name_en =="")&&(attrs.name_fr =="")) {
                      return "English and French names should be provided";
                    };
                     if (attrs.name_en =="") {
                      return "English name should be provided";
                    };
                    /* if (attrs.name_fr =="") {
                      return "French name should be provided";
                    };*/
                     /*if (attrs.groups =="") {
                      return "Item needs to be assigned to at least one group";
                    };*/
                  } ,           
                url: function () {
                    return this.get("id") ? itemsUrl + '/' + this.get("id") : itemsUrl;
                },
                clear: function () {
                    this.destroy();                    
                }
            });
})();
