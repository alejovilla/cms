/*global Backbone */
var app = app || {};

(function () {
	'use strict';
            var trans_url = "/transactions/icons";
            app.IconTransactionModel = Backbone.Model.extend({                
                url: function(){
                 return this.get('id') ? trans_url+'/'+this.get('id') : trans_url;
                },
             validate: function(attrs, options) {
                    var longc ,longm ,longy ,longk;

                    if ((attrs.code ==undefined)||(attrs.code =="")){
                      return "Icon code should be provided";
                    };
                    if ((attrs.name_en ==undefined)||(attrs.name_en =="")) {
                      return "English name should be provided";
                    };
                     if ((attrs.name_fr ==undefined)||(attrs.name_fr =="")) {
                      return "French name should be provided";
                    }; 
                    if ((attrs.name ==undefined)||(attrs.name =="")) {
                      return "Icon name should be provided";
                    };                  
                     if ((attrs.multiSItems ==undefined)||(attrs.multiSItems =={})) {
                      return "Icon needs to be assigned to at least one item";
                    };
                    if ((attrs.multiSLayouts ==undefined)||(attrs.multiSLayouts =={})) {
                      return "Icon needs to be assigned to at least one layout";
                    };
                    if (!((attrs.bagtagc ==undefined)&&(attrs.bagtagm ==undefined)&&(attrs.bagtagc ==undefined)&&(attrs.bagtagm ==undefined))) {
                          longc =  attrs.bagtagc.length>0;
                          longm =  attrs.bagtagm.length>0;
                          longy =  attrs.bagtagy.length>0;
                          longk =  attrs.bagtagk.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                           return "Please, review Bag Tag text colour";
                    };
                    if (!((attrs.binc ==undefined)&&(attrs.binm ==undefined)&&(attrs.binc ==undefined)&&(attrs.binm ==undefined))) {
                          longc =  attrs.binc.length>0;
                          longm =  attrs.binm.length>0;
                          longy =  attrs.biny.length>0;
                          longk =  attrs.bink.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Bin text colour";
                    };
                    if (!((attrs.bookc ==undefined)&&(attrs.bookm ==undefined)&&(attrs.bookc ==undefined)&&(attrs.bookm ==undefined))) {
                          longc =  attrs.bookc.length>0;
                          longm =  attrs.bookm.length>0;
                          longy =  attrs.booky.length>0;
                          longk =  attrs.bookk.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Book text colour";
                    };
                    if (!((attrs.canisterc ==undefined)&&(attrs.canisterm ==undefined)&&(attrs.canisterc ==undefined)&&(attrs.canisterm ==undefined))) {
                          longc =  attrs.canisterc.length>0;
                          longm =  attrs.canisterm.length>0;
                          longy =  attrs.canistery.length>0;
                          longk =  attrs.canisterk.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Canister text colour";
                    };
                    if (!((attrs.irononc ==undefined)&&(attrs.irononm ==undefined)&&(attrs.irononc ==undefined)&&(attrs.irononm ==undefined))) {
                         longc =  attrs.irononc.length>0;
                          longm =  attrs.irononm.length>0;
                          longy =  attrs.ironony.length>0;
                          longk =  attrs.irononk.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Iron on text colour";
                    };
                    if (!((attrs.psshoec ==undefined)&&(attrs.psshoem ==undefined)&&(attrs.psshoec ==undefined)&&(attrs.psshoem ==undefined))) {
                          longc =  attrs.psshoec.length>0;
                          longm =  attrs.psshoem.length>0;
                          longy =  attrs.psshoey.length>0;
                          longk =  attrs.psshoek.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review PPShoe text colour";
                    };
                    if (!((attrs.shoec ==undefined)&&(attrs.shoem ==undefined)&&(attrs.shoec ==undefined)&&(attrs.shoem ==undefined))) {
                          longc =  attrs.shoec.length>0;
                          longm =  attrs.shoem.length>0;
                          longy =  attrs.shoey.length>0;
                          longk =  attrs.shoek.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Shoe text colour";
                    };
                    if (!((attrs.stickyac ==undefined)&&(attrs.stickyam ==undefined)&&(attrs.stickyac ==undefined)&&(attrs.stickyam ==undefined))) {
                          longc =  attrs.stickyac.length>0;
                          longm =  attrs.stickyam.length>0;
                          longy =  attrs.stickyay.length>0;
                          longk =  attrs.stickyak.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Sticky (iant A) text colour";
                    };
                    if (!((attrs.stickybc ==undefined)&&(attrs.stickybm ==undefined)&&(attrs.stickybc ==undefined)&&(attrs.stickybm ==undefined))) {
                          longc =  attrs.stickybc.length>0;
                          longm =  attrs.stickybm.length>0;
                          longy =  attrs.stickyby.length>0;
                          longk =  attrs.stickybk.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Sticky (iant B) text colour";
                    };
                    if (!((attrs.stickycc ==undefined)&&(attrs.stickycm ==undefined)&&(attrs.stickycc ==undefined)&&(attrs.stickycm ==undefined))) {
                          longc =  attrs.stickycc.length>0;
                          longm =  attrs.stickycm.length>0;
                          longy =  attrs.stickycy.length>0;
                          longk =  attrs.stickyck.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Bag Tag text colour";
                    };
                    if (!((attrs.spicec ==undefined)&&(attrs.spicem ==undefined)&&(attrs.spicec ==undefined)&&(attrs.spicem ==undefined))) {
                          longc =  attrs.bagtagc.length>0;
                          longm =  attrs.bagtagm.length>0;
                          longy =  attrs.bagtagy.length>0;
                          longk =  attrs.bagtagk.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Sticky (iant A) text colour";
                    };
                    if (!((attrs.skinnyminic ==undefined)&&(attrs.skinnyminim ==undefined)&&(attrs.skinnyminic ==undefined)&&(attrs.skinnyminim ==undefined))) {
                         longc =  attrs.bagtagc.length>0;
                          longm =  attrs.bagtagm.length>0;
                          longy =  attrs.bagtagy.length>0;
                          longk =  attrs.bagtagk.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Skinny Mini text colour";
                    };
                    if (!((attrs.tagmatec ==undefined)&&(attrs.tagmatem ==undefined)&&(attrs.tagmatec ==undefined)&&(attrs.tagmatem ==undefined))) {
                          longc =  attrs.tagmatec.length>0;
                          longm =  attrs.tagmatem.length>0;
                          longy =  attrs.tagmatey.length>0;
                          longk =  attrs.tagmatek.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Tag Mate text colour";
                    };
                    if (!((attrs.teenytagc ==undefined)&&(attrs.teenytagm ==undefined)&&(attrs.teenytagc ==undefined)&&(attrs.teenytagm ==undefined))) {
                          longc =  attrs.teenytagc.length>0;
                          longm =  attrs.teenytagm.length>0;
                          longy =  attrs.teenytagy.length>0;
                          longk =  attrs.teenytagk.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Teeny Tag text colour";
                    };
                    if (!((attrs.wristbandc ==undefined)&&(attrs.wristbandm ==undefined)&&(attrs.wristbandc ==undefined)&&(attrs.wristbandm ==undefined))) {
                          longc =  attrs.wristbandc.length>0;
                          longm =  attrs.wristbandm.length>0;
                          longy =  attrs.wristbandy.length>0;
                          longk =  attrs.wristbandk.length>0;
                         if (!((longc==longm)&&(longk==longy)&&(longc==longy)))
                         return "Please, review Wrist Band text colour";
                    };
                  }                 
            });
})();
