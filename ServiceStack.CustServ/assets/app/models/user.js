/*global Backbone */
var app = app || {};

(function () {
	'use strict';
            app.UserModel = Backbone.Model.extend({
            urlRoot : "auth/",
            defaults: {
                isAuthenticated: false,
                hasRegistered: false,
                sessionId: null,
                userId: null,
                displayName: "",                
                form: null    
            },
            initialize: function () {
                _.bindAll(this, "loginSuccess", "loginError");
            },
            getProfile: function(){                          
                return this.fetch({url:"/users/"+this.get('sessionId')});              
            },
            signOut: function () {      
              this.set({ isAuthenticated: false });                     
            },
            isSignedIn: function(){             
              return app.currentUser.get('isAuthenticated')
            },
            login: function ($form) {
			    this.$form = $form;
                var options={};
                options.contentType = 'application/json';
                options.type = 'post';
                options.data = JSON.stringify(this.$form.serializeObject());
                options.url = this.urlRoot;
                self = this;
                options.success= this.loginSuccess; 
			    options.error= this.loginError;                                           
                this.fetch(options);   
            },
            loginSuccess: function(r) {
			this.set({ isAuthenticated: true });
		    },
		    loginError: function() {
			   $('.alert-danger',this.$form).show().find('span').html('Either username or password is incorrect');
		    },         
            hasPermissions: function (roles) {
                  var self = this; 
                  //return _.every(roles,function(role){ return self.get('permissions').indexOf(role)}); 
                   return _.every(roles,function(role){ return _.contains(self.get('permissions'), role)});
            }
        });
})();

