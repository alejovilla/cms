/*global Backbone */
var app = app || {};

(function () {
	'use strict';
           
            app.LayoutModel = Backbone.Model.extend({  
                urlRoot : "/layouts/",
                validate: function(attrs, options) {                   

                    if ((attrs.id ==undefined)||(attrs.id =="")){
                      return "Layout id should be provided";
                    };
                   /* if ((attrs.name ==undefined)||(attrs.name =="")) {
                      return "Layout name should be provided";
                    };*/                     
                }
                
            });
})();
