/*global Backbone */
var app = app || {};

(function () {
	'use strict';
    
            app.Many2ManyColoursModel = Backbone.Model.extend({                
                url: function(){
                 return 'colours/'+this.get('id')+'/many2many' ;
                },
             validate: function(attrs, options) {                    
                }                 
            });
})();
