/*global Backbone */
var app = app || {};

(function () {
	'use strict';
    
            app.Many2ManyIconsModel = Backbone.Model.extend({                
                url: function(){
                 return 'icons/'+this.get('id')+'/many2many' ;
                },
             validate: function(attrs, options) {                    
                }                 
            });
})();
