/*global Backbone */
var app = app || {};

(function () {
	'use strict';
           
            app.GroupModel = Backbone.Model.extend({                
                defaults: { 
                     name    : ""  
                },               
                validate: function(attrs, options) {                   

                    if ((attrs.name ==undefined)||(attrs.name =="")){
                      return "Group name should be provided";
                    };                                        
                }
                
            });
})();
