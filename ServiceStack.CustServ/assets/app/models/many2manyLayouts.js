/*global Backbone */
var app = app || {};

(function () {
	'use strict';
    
            app.Many2ManyLayoutsModel = Backbone.Model.extend({                
                url: function(){
                 return 'layouts/'+this.get('id')+'/many2many' ;
                },
             validate: function(attrs, options) {                    
                }                 
            });
})();
