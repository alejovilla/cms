/*global Backbone */
var app = app || {};

(function () {
	'use strict';
           
            app.LayoutSetModel = Backbone.Model.extend({                
                defaults: {
                      boxId : 0 ,
                      description :"",
                      top :0,
                      left :0,
                      bottom :0,
                      right:0                  
                }           
            });
})();
