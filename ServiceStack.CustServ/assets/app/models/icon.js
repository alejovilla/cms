/*global Backbone */
var app = app || {};

(function () {
	'use strict';
    
	var iconsUrl = "icons";      

             app.IconModel = Backbone.Model.extend({
                defaults: {
                      name_fr : "",
                      name_en : "",
                      isActive : 0                     
                },
                initialize: function () {                    
                },                
                url: function () {
                     return this.get("id") ? iconsUrl + '/' + this.get("id") : iconsUrl;
                },
                clear: function () {
                    this.destroy();
                    this.view.remove();
                }

            });
})();
