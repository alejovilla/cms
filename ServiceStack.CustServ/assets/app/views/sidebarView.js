/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {	'use strict';   
	app.SidebarView = Backbone.View.extend({		
	    prefix: "/templates/",
        template:"sidebar.html",
         serialize: function() {
           var capitaliseFirstLetter = function(string)
                {
                    return string.charAt(0).toUpperCase() + string.slice(1);
                }
            var structuredPermissions = {}, i , userPermissions = this.user.get('permissions');
            for(i=0;i<userPermissions.length;i++){
              var splitted = userPermissions[i].split("_");
              var element = {name:capitaliseFirstLetter(splitted[1])+" "+splitted[0],link:splitted[0]+'/'+splitted[1]};
              if (structuredPermissions[capitaliseFirstLetter(splitted[0])]==undefined)
                 structuredPermissions[capitaliseFirstLetter(splitted[0])] = [element];
              else
                 structuredPermissions[capitaliseFirstLetter(splitted[0])].push(element);
            }
            var finalPermissions=Array();							
			for (var prop in structuredPermissions) {               
				finalPermissions.push({area:prop,actions:structuredPermissions[prop]});
				}
            return {operation: this.operation, permissions:finalPermissions};
        } 
     });
})(jQuery);
