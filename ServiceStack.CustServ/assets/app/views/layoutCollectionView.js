/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {	'use strict';   
	app.LayoutCollectionView = Backbone.View.extend({		
	    prefix: "/templates/",
        template:"layout_list.html", 
        initialize : function(options) {
         self  = this;
         this.collection.on('add chage reset remove', self.render,self);
         },         
        afterRender: function(){
          FormComponents.refresh();
        }, 
        getLayouts: function (itemsIds) {
                this.collection.getLayoutsByItems(itemsIds);
                return this;                   
                }  ,
        getAllLayouts: function () {
                this.collection.fetch();
                return this;                   
                }, 		        
        serialize: function() {
            return this.collection ? this.collection.toJSON() : [];
        }  
     });
})(jQuery);
