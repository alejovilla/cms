/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};
(function ($) {
	'use strict';

	app.StdColours_workplaceView = Backbone.View.extend({
	    prefix: "/templates/",
		 template: "stdColours_workplace.html",
         events :{
          "click  #many2manyColoursBtn"  : "getMany2Manytables",
          "submit  form"            : "updateColour",
          "click  #itemsBtn"        : "updateItems",
          "click  #layoutBtn"      : "updateLayouts",
          "click  .btn.default"     : "cancel"
        },  
         
         initialize: function(){
          this.colourid   = null,
          this.colourid = null,
          this.itemid   = null,
          this.many2many = new app.Many2ManyColoursModel();
         },
         getMany2Manytables: function(event){
          if(event) event.preventDefault();  
           this.many2many.set({id:$('#many2manycolours').select2("val")});
           var self = this;
           this.many2many.fetch().success(function(){              
              self.render();
           }); 
           
         }, 
          updateColour : function(event){
          event.preventDefault();          
          var values = this.$el.find('form').serializeObject();
          values['id']            = values['many2manycolours'] ; // to force a PUT
          values['isActive']      = values['isActive'] !==undefined;
          values['MultiSLayouts']   = typeof(values['m2mMultiSLayouts[]'])=="string" ? Array(values['m2mMultiSLayouts[]']) : values['m2mMultiSLayouts[]']; 
          values['MultiSItems']   = typeof(values['m2mMultiSItems[]'])=="string" ? Array(values['m2mMultiSItems[]']) : values['m2mMultiSItems[]'];
          
          app.M2MItem =  Backbone.Model.extend();
          var trans_item = new app.M2MItem();                       
          var self = this;
          trans_item.save(values,{url:"/colours/"+values['id']+"/many2many", success:self.onSuccess, error: self.onError});        
        },
		onSuccess : function(){
                toastr.success("Many2Many and Colour tables successfully updated", "<strong>You're done!</strong>");  
                $("#tab_main").click();
                },
        onError : function(){
                toastr.error("Many2Many and Colour table were not successfully updated.", "<strong>Oops! Something went wrong...</strong>");
        },        
         updateItems: function(event){
           event.preventDefault(); 
           var itemID = $("#many2manyitems").val();
           var newItems = new app.ItemList();
           newItems.fetch({url: "/colours/"+itemID+"/many2many/items",
                                       success: function() {
                                          var layHash = newItems.toJSON();
                                           layHash = _.map(layHash,function(item){return item.code;});
                                           $('#m2mMultiSItems').multiSelect('deselect_all').multiSelect('select',layHash);
                                         }
                                       });             
         },
         updateLayouts: function(event){
           event.preventDefault(); 
           var layoutID = $("#many2manylayouts").val();
           var newLayouts = new app.LayoutList();
           newLayouts.fetch({url: "/colours/"+layoutID+"/many2many/layouts",
                                       success: function() {
                                          var layHash = newLayouts.toJSON();
                                           layHash = _.map(layHash,function(layout){return layout.id.toString();});
                                           $('#m2mMultiSLayouts').multiSelect('deselect_all').multiSelect('select',layHash);
                                         }
                                       });             
         },
         cancel : function(event){
            event.preventDefault(); 
            this.render();
         },
         serialize: function() {
            var many2manyToRender = this.many2many.toJSON();
            many2manyToRender.items = this.items.toJSON();            
            many2manyToRender.colours = this.colours.toJSON();
            many2manyToRender.layouts = this.templates.toJSON();  
            many2manyToRender.itemid   = this.itemID;
            many2manyToRender.layoutid   = this.layoutID;
            many2manyToRender.operation = "Standard Colours view";
            return many2manyToRender; 
        },
        afterRender: function(){         
          FormComponents.init();
          Many2ManyWorkplace.populateUpdateColours(this.many2many.toJSON());
          App.initAjax();           
        }, 		
	});
})(jQuery);
