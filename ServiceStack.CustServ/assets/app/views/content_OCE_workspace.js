/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {	'use strict';   
	app.ContentOCEWorkspaceView = Backbone.View.extend({		
	    prefix: "/templates/",
        template:"content_order_conf.html",
        events :{ 
          "submit  form"      : "updateOCEcontent"
        },        
                    
        updateOCEcontent : function(event){
          event.preventDefault();
          var self= this;          
          var texts = this.$el.find('form').serializeObject();           
          this.orderConfEmailEn1.save({value:texts.oceeditoren},{success:self.onSuccess, error: self.onError});
          this.orderConfEmailFr1.save({value:texts.oceeditorfr},{success:self.onSuccess, error: self.onError}); 
		  this.orderConfEmailEn2.save({value:texts.oceeditorenbtm},{success:self.onSuccess, error: self.onError});
          this.orderConfEmailFr2.save({value:texts.oceeditorfrbtm},{success:self.onSuccess, error: self.onError});           
        },
         onSuccess : function(){
                toastr.success("Order confirmation email content successfully udpated", "<strong>You're done!</strong>");
                },
        onError : function(){
                toastr.error("Order confirmation email content was not udpated", "<strong>Oops! Something went wrong...</strong>");
        }, 
        afterRender: function(){         
          FormComponents.init();
          FormEditable.init();
          App.initAjax();           
        },
        serialize: function() {
            var infoToShow = {};
            infoToShow.operation = this.operation; 
            infoToShow.orderConfEmailFr1 = this.orderConfEmailFr1.get('value');
            infoToShow.orderConfEmailEn1 = this.orderConfEmailEn1.get('value');
			infoToShow.orderConfEmailFr2 = this.orderConfEmailFr2.get('value');
            infoToShow.orderConfEmailEn2 = this.orderConfEmailEn2.get('value');
            return infoToShow; 
        } 
     });

})(jQuery);