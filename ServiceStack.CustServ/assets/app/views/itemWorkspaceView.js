/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {	'use strict';   
	app.ItemWorkspaceView = Backbone.View.extend({		
	    prefix: "/templates/",
        template:"item_workplace.html",
        events :{
          "click .dropdown-menu :checkbox" : "update",
          "keyup :text#letters"            : "update",
        },
        update: function (){
          var _filters = {};
          if($(":checkbox#core").is(":checked"))
             _filters['isCoreItem'] = true;
         if($(":checkbox#active").is(":checked"))
             _filters['isActive'] = true;
        if($(":checkbox#shipping").is(":checked"))
             _filters['type'] = "Shipping";
        if($(":text#letters").val()!="")
              _filters['name'] = $(":text#letters").val();
         app.view('#item_list').filteredRender(_filters);
        },
         afterRender:function() {
                      this.item_listView.render();
                      $('.well').show();
                    },
        serialize: function() {
            return {operation: this.operation};
        } 
     });
})(jQuery);
