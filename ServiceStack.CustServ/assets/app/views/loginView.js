/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {	'use strict';   
	app.LoginView = Backbone.View.extend({		
	    prefix: "/templates/",
        template:"login.html",
        events: {			
			'submit form': 'login'			
		},
        initialize: function(){
        
        },
        login: function(e) {
            this.model.on("change:isAuthenticated",this.startApp);
			if (e) e.preventDefault();
			this.model.login(this.$("form"));            
		},
        startApp:function(){
           this.getProfile().success(function(){
              app.router.navigate('home', true);
           });
			$('.alert-danger',this.$form).hide().find('span').html('Enter any username and password');
            $('.alert-success',this.$form).show();
        }
            
     });
})(jQuery);
