/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {	'use strict';   
	app.CategoryCollectionView = Backbone.View.extend({		
	    prefix: "/templates/",
        template:"category_list.html",      
       events :{         
          "click .deleteCategory" : "deleteCategory"          
        },  
       initialize: function(){ 
       },  
		
		deleteCategory : function(event){
          event.preventDefault();
		  var nameToDelete= $(event.target).attr("todelete");
          var self= this; 
		  if (confirm("Do you really want to delete this category?")){
			  $.ajax({
				url: "/items/groups/"+nameToDelete,
				type: "DELETE",
				//data:{name:"+},
				success: function(result) {
					self.onSuccess(event);
				},
				error: function(object, reason, anotherObjt){
					self.onError();
				},
			});
		  }
		},
		onSuccess : function(event){
                toastr.success("Category successfully delete", "<strong>You're done!</strong>");
				var nameToDelete= $(event.target).attr("todelete");
				this.collection.remove(this.collection.findWhere({name:nameToDelete}),{silent:true});
                this.render();
                },
        onError : function(){
                toastr.error("Category was not successfully delete", "<strong>Oops! Something went wrong...</strong>");
        },
        serialize: function() {
           return this.collection.toJSON();           
        },
        
        afterRender: function(){
           FormEditable.init();
           FormComponents.init(); 
        }  
     });
})(jQuery);
