/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {	'use strict';   
	app.ItemCollectionView = Backbone.View.extend({		
	    prefix: "/templates/",
        template:"item_list.html",      
       
       initialize: function(){         
         this.filters={};
          },
        filterCollection: function(){
                if(_.isEmpty(this.filters)) 
                     return this.collection.toArray();

                var search = this.filters["name"];               
                delete this.filters["name"];
                var preFiltered = (_.isEmpty(this.filters)) ? this.collection.toArray() : this.collection.where(this.filters);

                if(search==undefined) 
                   return preFiltered;
                else {
		                var pattern = new RegExp(search,"gi");
		                return _.filter(preFiltered, function(model) {return pattern.test(model.get("name"));});
                    }
        },
       filteredRender: function(_filters){
          this.filters = _filters;
          this.render();
       },
       groupItems : function(models){							
							var indexi, indexj,collectionByGroups={};
							for (indexi = 0; indexi < models.length; ++indexi) {
								var categories = models[indexi].get('groups');
								if ((categories==undefined)||(categories==""))
                                    categories = "NO CATEGORY";
								var arrayCat = categories.split(",");
								for (indexj = 0; indexj < arrayCat.length; ++indexj) {
								if (collectionByGroups[arrayCat[indexj]]==undefined){
									collectionByGroups[arrayCat[indexj]] = [models[indexi].toJSON()];
								}
								else {										   
									collectionByGroups[arrayCat[indexj]].push(models[indexi].toJSON());
								};
                                collectionByGroups[arrayCat[indexj]].sort(function(a,b){return a.name>=b.name ? 1 : -1});
								}
								
							};
                            var finalCollection=Array();							
						    for (var prop in collectionByGroups) {
                                var _linkId = prop.replace(" ","_").replace("(","_").replace(")","_");
								finalCollection.push({name:prop,linkId:_linkId,items:collectionByGroups[prop],itemcount:collectionByGroups[prop].length});
							  }
                                finalCollection.sort(function(a,b){return a.name>=b.name ? 1 : -1}); 
                            return finalCollection;                             
				},		        
        serialize: function() {
            var filteredCollection = this.filterCollection();
            return {grouped: this.groupItems(filteredCollection)};
        }  
     });
})(jQuery);
