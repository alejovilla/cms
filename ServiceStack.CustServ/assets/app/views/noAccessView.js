/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {	'use strict';   
	app.NoAccessView = Backbone.View.extend({		
	    prefix: "/templates/",
        template:"noaccess.html"         
     });
})(jQuery);
