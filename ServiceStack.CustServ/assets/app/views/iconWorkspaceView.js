/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {	'use strict';   
	app.IconWorkspaceView = Backbone.View.extend({		
	    prefix: "/templates/",
        template:"icon_ins_workplace.html",
        events :{
          "keyup #codeIcon"            : "checkUniqueness",
          "click  .button-next,.button-previous,#tab3link" : "checkForAction",
          "change #multiSItems"       : "selectionChanged",
          "submit  form"              : "createIcon",
          "click  #iconbtnPickup"     : "fillItems",
          "click  #iconLayoutBtnPickup": "fillLayouts"
        },  
        
        checkUniqueness : function(){
          var dummy = new app.IconModel({id:$("#codeIcon").val()});
          var self= this;
          dummy.fetch({success:self.codeNotUnique,error:self.codeUnique});
        },
        codeNotUnique: function(){
           $("#codeIcon").attr('unique',false).valid();
        },
        codeUnique: function(){
           $("#codeIcon").attr('unique',true).valid();
        },
        selectionChanged: function(){
          this.multiSItemsChanged = true;
        },
        checkForAction : function (event) {
         var currentTab = $('.tab-pane.active');        
         var step =  currentTab.attr('id');
         if (('tab3'==step || event.currentTarget.id == "tab3link")&&(this.multiSItemsChanged)) {           
           var selection = $('#multiSItems').val();
           this.layoutView.getLayouts(selection);
           this.multiSItemsChanged = false;
         }
        },
         createIcon : function(event){
          event.preventDefault();          
          var values = this.$el.find('form').serializeObject();
          values['isActive'] = values['isActive'] !==undefined;
          values['multiSItems']   = typeof(values['multiSItems[]'])=="string" ? Array(values['multiSItems[]']) : values['multiSItems[]']; 
          values['multiSLayouts']   = typeof(values['multiSLayouts[]'])=="string" ? Array(values['multiSLayouts[]']) : values['multiSLayouts[]'];
          this.transaction.clear({silent:true});
          var self = this;
          this.transaction.save(values,{success:self.onSuccess, error: self.onError});        
        },
        onSuccess : function(){
                toastr.success("Icon successfully inserted", "<strong>You're done!</strong>");      
                IconWorkplace.clearForm();
                $("#toUpdateIcon").select2("val", "");
                $("#startTab").click();
                },
        onError : function(){
                toastr.error("Icon was not successfully.", "<strong>Oops! Something went wrong...</strong>");
        },
        fillItems : function(event){
          event.preventDefault();          
           var iconID = $("#iconPickup").val();
           var tempItemCollection = new app.ItemList();
           tempItemCollection.fetch({url: "/icons/"+iconID+"/items/",
                                       success: function() {
                                          var layHash = tempItemCollection.toJSON();
                                           layHash = _.map(layHash,function(item){return item.code.toString();});
                                           $('#multiSItems').multiSelect('deselect_all');
                                           $('#multiSItems').multiSelect('select',layHash);
                                         }
                                       });           
        },
        fillLayouts : function(event){
          event.preventDefault(); 
           var iconID = $("#iconlayoutPickup").val();
           var tempLayoutCollection = new app.LayoutList();
           tempLayoutCollection.fetch({url: "/icons/"+iconID+"/layouts/",
                                       success: function() {
                                          var layHash = tempLayoutCollection.toJSON();
                                           layHash = _.map(layHash,function(layout){return layout.id.toString();});
                                           $('#multiSLayouts').multiSelect('deselect_all');
                                           $('#multiSLayouts').multiSelect('select',layHash);
                                         }
                                       });           
        },            
        serialize: function() {
            var infoToShow = {};            
            infoToShow.items     = this.itemCollection.toJSON();
            infoToShow.operation = this.operation;
            infoToShow.icons     = this.iconCollection.toJSON();
            return infoToShow; 
        } 
     });

     app.IconInsertWorkspaceView = app.IconWorkspaceView.extend({
       initialize : function(options){
          this.multiSItemsChanged = false;        
          this.transaction = new app.IconTransactionModel();
          this.transaction.on("invalid", function(model, error) {
          toastr.error(error, "<strong>Oops! Something went wrong...</strong>");
        }); 
           $(":radio").closest("span").addClass("checked"); 
        },
        afterRender: function(){
          FormWizard.init();
          FormComponents.init();         
        } 
     });
     app.IconUpdateWorkspaceView = app.IconWorkspaceView.extend({     
     template:"icon_upd_workplace.html",

     events :{
          "click #toUpdateIconBtn" : "afterIconSelection",
           "submit  form"          :  "updateIcon",
           "click  #iconbtnPickup"     : "fillItems",
          "click  #iconLayoutBtnPickup": "fillLayouts"
        }, 

       initialize : function(options){
          this.multiSItemsChanged = false;
          this.transaction = new app.IconTransactionModel();
          this.transaction.on("invalid", function(model, error) {
          toastr.error(error, "<strong>Oops! Something went wrong...</strong>");
        });                      
        },        
        afterIconSelection:function(event){
           if(event) event.preventDefault();            
           var iconSelected = $("#toUpdateIcon").val();
           this.transaction.set({id:iconSelected});
           var self= this;
           this.transaction.fetch().success(function(){
              IconWorkplace.populateUpdate(self.transaction);
           });          
        },
        checkForAction : function () {         
        },
        updateIcon : function(event){
          event.preventDefault();          
          var values = this.$el.find('form').serializeObject();
          values['id']            = values['toUpdateIconCode'] ; // to force a PUT
          values['code']          = values['toUpdateIconCode'] ; 
          values['isActive']      = values['isActive'] !==undefined;
          values['multiSItems']   = typeof(values['multiSItems[]'])=="string" ? Array(values['multiSItems[]']) : values['multiSItems[]']; 
          values['multiSLayouts']   = typeof(values['multiSLayouts[]'])=="string" ? Array(values['multiSLayouts[]']) : values['multiSLayouts[]'];
          this.transaction.clear({silent:true});
          var self = this;
          this.transaction.save(values,{success:self.onSuccess, error: self.onError});        
        },
		onSuccess : function(){
                toastr.success("Icon successfully inserted", "<strong>You're done!</strong>");  
                $("#startTab").click();
                },
        afterRender: function(){
          FormWizard.init();
          FormComponents.init();           
        },
        serialize: function() {
            var infoToShow = {};            
            infoToShow.items     = this.itemCollection.toJSON();
            infoToShow.operation = this.operation;
            infoToShow.update    = true;
            infoToShow.icons     = this.iconCollection.toJSON();
            infoToShow.layouts   = this.layoutCollection.toJSON();
            return infoToShow; 
        } 
     });

})(jQuery);