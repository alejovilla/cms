/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {	'use strict';   
	app.LayoutWorkspaceView = Backbone.View.extend({		
	    prefix: "/templates/",
        template:"test2.html",
        events :{ 
          "submit  form"      : "updateLayout",
          "click #layoutBtn"  : "afterLayoutSelection"
        },        
       
       initialize : function(options){
          this.multiSItemsChanged = false;
          this.layout = new app.LayoutModel();
          var self= this;          
          this.layout.on("invalid", function(model, error) {
          toastr.error(error, "<strong>Oops! Something went wrong...</strong>");          
        });                      
        },        
        afterLayoutSelection:function(event){
           if(event) event.preventDefault();            
           var layoutSelected = $("#toUpdateLayout").val();
           this.layout.set({id:layoutSelected});
           var self= this;
           this.layout.fetch().success(function(){              
              self.render();
           }); 
                   
        },                
        updateLayout : function(event){
          event.preventDefault();          
          var values = this.$el.find('form').serializeObject(); 
          values['isActive']      = values['isActive'] !==undefined;
          this.layout.clear({silent:true});          
          var template = new app.LayoutModel({id:values.id,  name :values.name, score :values.score,  isActive : values.isActive, printerMaterial: values.printerMaterial});
          
          var self = this;
          this.layout.save({id:values.id, template:template},{success:self.onSuccess, error: self.onError});        
        },
         onSuccess : function(){
                toastr.success("Layout successfully udpated", "<strong>You're done!</strong>");
                },
        onError : function(){
                toastr.error("Layout was not udpated", "<strong>Oops! Something went wrong...</strong>");
        }, 
        afterRender: function(){         
          FormComponents.init();
          FormComponents.diplayTags(this.layout.get('boxes'));
          App.initAjax();           
        },
        serialize: function() {
            var infoToShow = {};
            infoToShow.layout = this.layout.toJSON();
            infoToShow.operation = this.operation;  
            infoToShow.layouts   = this.layoutCollection.toJSON();
            return infoToShow; 
        } 
     });

})(jQuery);