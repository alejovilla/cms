/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {	'use strict';   
	app.CategoryWorkspaceView = Backbone.View.extend({		
	    prefix: "/templates/",
        template:"category_workplace.html",
        serialize: function() {
        },
        afterRender:function() {
                      this.cat_listView.$el.appendTo("#cat_list");
                      this.cat_listView.render();                         
                    }, 
     });

    

})(jQuery);