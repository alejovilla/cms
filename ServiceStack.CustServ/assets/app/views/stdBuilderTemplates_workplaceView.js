/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};
(function ($) {
	'use strict';

	app.StdBuilderTemplates_workplaceView = Backbone.View.extend({
	    prefix: "/templates/",
		 template: "stdBuilderTemplates_workplace.html",
         events :{
          "click  #many2manyLayoutBtn"  : "getMany2Manytables",
           "submit  form"              : "updateLayout",
          "click  #iconsBtn"       : "updateIcons",
          "click  #coloursBtn"      : "updateColours",
          "click  #itemsBtn"      : "updateItems",
          "click  .btn.default"     : "cancel"
        },  
         
         initialize: function(){
          this.iconid   = null,
          this.colourid = null,
          this.itemid   = null,
          this.many2many = new app.Many2ManyLayoutsModel();
         },
         getMany2Manytables: function(event){
          if(event) event.preventDefault();  
           this.many2many.set({id:$('#many2manylayouts').select2("val")});
           var self = this;
           this.many2many.fetch().success(function(){              
              self.render();
           }); 
           
         },
         updateLayout : function(event){
          event.preventDefault();          
          var values = this.$el.find('form').serializeObject();
          values['id']            = values['many2manylayouts'] ; // to force a PUT
          values['isActive']      = values['isActive'] !==undefined;
          values['MultiSIcons']   = typeof(values['m2mMultiSIcons[]'])=="string" ? Array(values['m2mMultiSIcons[]']) : values['m2mMultiSIcons[]']; 
          values['MultiSColours'] = typeof(values['m2mMultiSColours[]'])=="string" ? Array(values['m2mMultiSColours[]']) : values['m2mMultiSColours[]'];
          values['MultiSItems']   = typeof(values['m2mMultiSItems[]'])=="string" ? Array(values['m2mMultiSItems[]']) : values['m2mMultiSItems[]'];
          
          app.M2MItem =  Backbone.Model.extend();
          var trans_item = new app.M2MItem();                       
          var self = this;
          trans_item.save(values,{url:"/layouts/"+values['id']+"/many2many", success:self.onSuccess, error: self.onError});        
        },
		onSuccess : function(){
                toastr.success("Many2Many tables and Layout successfully updated", "<strong>You're done!</strong>");  
                $("#tab_main").click();
                },
        onError : function(){
                toastr.error("Many2Many tables or Layout were not successfully updated.", "<strong>Oops! Something went wrong...</strong>");
        },
         updateIcons: function(event){
           event.preventDefault(); 
           var itemID = $("#many2manyicons").val();
           var newIcons = new app.IconList(); 
           newIcons.fetch({url: "/layouts/"+itemID+"/many2many/icons",
                                       success: function() {
                                          var layHash = newIcons.toJSON();
                                           layHash = _.map(layHash,function(icon){return icon.id.toString();});
                                           $('#m2mMultiSIcons').multiSelect('deselect_all').multiSelect('select',layHash);
                                         }
                                       });             
         },
         updateColours: function(event){
           event.preventDefault(); 
           var itemID = $("#many2manycolours").val();
           var newColours = new app.ColourList();
           newColours.fetch({url: "/layouts/"+itemID+"/many2many/colours",
                                       success: function() {
                                          var layHash = newColours.toJSON();
                                           layHash = _.map(layHash,function(colour){return colour.id.toString();});
                                           $('#m2mMultiSColours').multiSelect('deselect_all').multiSelect('select',layHash);
                                         }
                                       });             
         },
         updateItems: function(event){
           event.preventDefault(); 
           var itemID = $("#many2manyitems").val();
           var newItems = new app.ItemList();
           newItems.fetch({url: "/layouts/"+itemID+"/many2many/items",
                                       success: function() {
                                          var layHash = newItems.toJSON();
                                           layHash = _.map(layHash,function(item){return item.code;});
                                           $('#m2mMultiSItems').multiSelect('deselect_all').multiSelect('select',layHash);
                                         }
                                       });             
         },
         
         cancel : function(event){
            event.preventDefault(); 
            this.render();
         },
         serialize: function() {
            var many2manyToRender = this.many2many.toJSON();
            many2manyToRender.items = this.items.toJSON();
            many2manyToRender.icons = this.icons.toJSON(); 
            many2manyToRender.colours = this.colours.toJSON(); 
            many2manyToRender.layouts = this.templates.toJSON();                   
            many2manyToRender.iconid   = this.iconid ;
            many2manyToRender.colourid = this.colourid;
            many2manyToRender.itemid   = this.tempid ;
            many2manyToRender.operation = "Standard Items view";
            return many2manyToRender; 
        },
        afterRender: function(){         
          FormComponents.init();
          Many2ManyWorkplace.populateUpdateLayouts(this.many2many.toJSON());
          App.initAjax();           
        }, 		
	});
})(jQuery);
