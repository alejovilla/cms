/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};
(function ($) {
	'use strict';

	app.ItemView = Backbone.View.extend({
	    prefix: "/templates/",
		 template: "item_detail.html",
		events: {			
			'submit form': 'save',
			'keypress .edit': 'updateOnEnter',
            'click .cancel' : "cancel"
		},
        initialize : function(){   
         this.model.on("invalid", function(model, error) {
          toastr.error(error, "<strong>Oops! Something went wrong...</strong>")
        });          
        },
		serialize: function() {
                        var preItem= this.model.toJSON();                       
                        if (preItem.lastWeighed){
                          var lastWeighed = new Date(parseInt(preItem.lastWeighed.substring(6)));
                          preItem.lastWeighed = [lastWeighed.getFullYear(),lastWeighed.getMonth()+1, lastWeighed.getDate()].join('-')+" "+lastWeighed.toLocaleTimeString();
                          }
                        else
                          preItem.lastWeighed = "";
                        return preItem;
                      } ,
	    afterRender:function() {
                      FormComponents.init();
                      FormValidation.init();
                      $('.well').hide();
                    },
        save: function(event){
          event.preventDefault();          
          var values = this.$el.find('form').serializeObject();
          values['isActive'] = values['isActive'] !==undefined;
          values['isCoreItem'] = values['isCoreItem']!==undefined;
          values['isSale'] = values['isSale']!==undefined;
          values['applyCoupon'] = values['applyCoupon']!==undefined;
          this.model.save(values,{wait:true, success: function(){
                                                        app.view('#item_list').render();
                                                        toastr.success("Item successfully updated", "<strong>You're done!</strong>") 
                                                       }
          });
        },
        cancel: function(){
           $(".well").show();
           app.layout.removeView('#item_detail');           
        }
	});
})(jQuery);
