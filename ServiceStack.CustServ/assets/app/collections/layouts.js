/*global Backbone */
var app = app || {};

(function () {
	'use strict';
            var  items_layouts_url = "items/layouts/allmatches";
            var url_Root= "/layouts/";     
            app.LayoutList = Backbone.Collection.extend({
                model: app.LayoutModel,
                url: function(){
                  return url_Root;
                },                      
                getLayoutsByItems: function (itemsIdsList) {
                var options={};
                options.contentType = 'application/json';
                options.type = 'post';
                options.data = JSON.stringify({itemsIds:itemsIdsList});
                options.url = items_layouts_url;
                options.reset = true; 
                return this.fetch(options);                   
                }
            });
    app.Layouts = new app.LayoutList(); 
})();