/*global Backbone */
var app = app || {};

(function () {
	'use strict';
     var coloursUrl = "/colours/";
      // Colour Collection 
        app.ColourList= Backbone.Collection.extend({
                model: app.ColourModel,                
                url:function(){ 
                    return !this.itemID ? coloursUrl : "/items/" + this.itemID + "/"+coloursUrl
                },
                initialize: function(models, options) {
                    options || (options = {});
                    if (options.itemID!== undefined) {
                        this.itemID = options.itemID;
                    };
                }
	});
	app.Colours = new app.ColourList([],{itemID:null});
})();
