/*global Backbone */
var app = app || {};

(function () {
	'use strict';
                 
            app.GroupList = Backbone.Collection.extend({
                model: app.GroupModel,
                urlRoot : "items/groups", 
                url: function(){
                  return this.urlRoot;
                },                   
                renameGroup: function (toReplace, replaceWith) {
                var options={};
                options.contentType = 'application/json';
                options.type = 'put';
                options.data = JSON.stringify({categoryToFind:toReplace, categoryToReplaceWith :replaceWith });
                options.url = url_Root;
                options.reset = true; 
                return this.fetch(options);                   
                }
            });
    app.Groups = new app.GroupList(); 
})();