/*global Backbone */
var app = app || {};

(function () {
	'use strict';

            app.LayoutList = Backbone.Collection.extend({
                model: app.LayoutSetIconModel
            }); 
})();