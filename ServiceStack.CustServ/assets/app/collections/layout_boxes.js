/*global Backbone */
var app = app || {};

(function () {
	'use strict';
    
            app.LayoutBoxList = Backbone.Collection.extend({
                model: app.LayoutBoxModel
            }); 
})();