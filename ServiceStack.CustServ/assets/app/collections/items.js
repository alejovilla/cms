/*global Backbone */
var app = app || {};

(function () {
	'use strict';
	var itemsUrl = "/items/";    
     // Item Collection
            app.ItemList = Backbone.Collection.extend({            
                model: app.ItemModel,
                url:function(){ 
                    return itemsUrl;
                }
	});
	app.Items = new app.ItemList();   
})();

