/*global Backbone */
var app = app || {};

(function () {
	'use strict';
     var iconsUrl = "/icons/";
      // Colour Collection 
           // Icons Collection 
            app.IconList= Backbone.Collection.extend({
                model: app.IconModel,
                url: function(){
				  return iconsUrl;
				}
	});

    app.Icons = new app.IconList();   

})();
